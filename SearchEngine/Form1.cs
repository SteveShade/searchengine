﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SearchEngine.Properties;

namespace SearchEngine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int numberFile = 1;
        private int massFilesSize;
        private List<string> forSave = new List<string>();
        private bool pause = false;
        private DateTime time;

        /// <summary>
        /// Проверка текстовых полей "Путь" и "Имя файла"
        /// </summary>
        private void Check()
        {
            if ((directory.Text == "") || (fileName.Text == ""))
            {
                search.Enabled = false;
            }
            else
            {
                search.Enabled = true;
                if ((pauseButton.Text == "Пауза") && (pauseButton.Visible == true))
                {
                    search.PerformClick();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            FBD.ShowNewFolderButton = false;
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                directory.Text = FBD.SelectedPath;
            }

            if ((pauseButton.Text == "Пауза") && (pauseButton.Visible == true))
            {
                search.PerformClick();
            }
        }

        private void directory_TextChanged(object sender, EventArgs e)
        {
            Check();
        }

        private void fileName_TextChanged(object sender, EventArgs e)
        {
            Check();
        }

        private void content_TextChanged(object sender, EventArgs e)
        {
            Check();
        }

        private async void search_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(directory.Text))
            {
                MessageBox.Show("Заданная директория не существует. Проверьте правильность введеной директории.");
            }
            else
            {
                if ((pauseButton.Text == "Пауза") && (pauseButton.Visible == true))
                {
                    PauseClick(pauseButton);
                }
                treeView1.Nodes.Clear();
                numberFile = 1;
                time = new DateTime();
                await Task.Run(() => SearchButton());
            }
        }

        /// <summary>
        /// Отрисовка и настройка интерфейса интерфейса. Запуск поиска.
        /// </summary>
        private void SearchButton()
        {


            List<string> dirs = GetFiles(directory.Text, "*.*");
            massFilesSize = dirs.Count;
            
            BeginInvoke(new MethodInvoker(delegate
            {
                progressBar.Minimum = 1;
                progressBar.Maximum = dirs.Count;
                progressBar.Value = 1;
                progressBar.Step = 1;
                progressBar.Visible = true;

                labelForFilename.Width = 380;
                labelForFilename.Visible = true;

                labelForFileNumber.Width = 250;
                labelForFileNumber.Visible = true;

                labelTimer.Visible = true;

                pauseButton.Height = 30;
                pauseButton.Text = "Пауза";
                pauseButton.Visible = true;

                timer.Start();
            }));

            System.Threading.Thread.Sleep(20);
            SearchFiles(dirs);
        }

        /// <summary>
        /// Поиск файлов и директорий по названию и по содержимому.
        /// </summary>
        /// <param name="dirs"> Список всех файлов и директорий для поиска. </param> 
        private void SearchFiles(List<string> dirs)
        {
            string fileContent;
            treeView1.PathSeparator = @"\";
            Color nameColor = Color.FromArgb(112, 150, 212);
            Color contentColor = Color.FromArgb(52, 179, 124);
            forSave.AddRange(dirs);

            foreach (string file in dirs)
            {
                BeginInvoke(new MethodInvoker(delegate
                {
                    labelForFileNumber.Text = numberFile.ToString() + " из " + progressBar.Maximum.ToString();
                    labelForFilename.Text = file;                   
                }));

                System.Threading.Thread.Sleep(20);

                if (pauseButton.Text == "Продолжить")
                {
                    break;
                }

                string[] onlyFile = file.Split('\\');
                if (onlyFile[onlyFile.Length - 1].IndexOf(fileName.Text, StringComparison.OrdinalIgnoreCase) != -1)
                {
                Tree(treeView1, file, '\\', nameColor);

                BeginInvoke(new MethodInvoker(delegate
                {
                    progressBar.PerformStep();
                    numberFile++;
                }));

                continue;

                }
                try
                {
                    fileContent = File.ReadAllText(file);
                }
                catch (IOException)
                {
                    BeginInvoke(new MethodInvoker(delegate
                    {
                        progressBar.PerformStep();
                        numberFile++;
                    }));
                    continue;
                }

                if (fileContent.Contains(content.Text))
                {
                    Tree(treeView1, file, '\\', contentColor);
                    BeginInvoke(new MethodInvoker(delegate
                    {
                        progressBar.PerformStep();
                        numberFile++;
                    }));
                    continue;
                }
                BeginInvoke(new MethodInvoker(delegate
                {
                    progressBar.PerformStep();
                    numberFile++;
                }));

                forSave.Remove(file);
            }
            if (numberFile >= massFilesSize)
            {
                timer.Stop();
                MessageBox.Show("Поиск завершен");
                BeginInvoke(new MethodInvoker(delegate
                {
                    numberFile = 1;
                    progressBar.Visible = false;
                    labelForFilename.Visible = false;
                    labelForFileNumber.Visible = false;
                    pauseButton.Visible = false;
                    pause = false;                  
                    labelTimer.Visible = false;
                }));
            }

        }

        /// <summary>
        /// Остановка процесса поиска.
        /// </summary>
        /// <param name="button"> Button отвечающий за паузу.</param>
        private async void PauseClick(Button button)
        {
            if (button.Text == "Пауза")
            {
                button.Text = "Продолжить";
                pause = true;
                timer.Stop();
            }
            else
            {
                button.Text = "Пауза";
                pause = false;
                List<string> dirs = forSave.GetRange(0, forSave.Count);
                timer.Start();
                await Task.Run(() => SearchFiles(dirs));
            }
        }

        /// <summary>
        /// Функция отвечающая за корректную отрисовку дерева найденых файлов и директорий.
        /// </summary>
        /// <param name="treeView"> Объект TreeView на котором отрисовывается дерево.</param>
        /// <param name="path">Файл, который будет отображен в дереве.</param>
        /// <param name="pathSeparator">Разделитель</param>
        /// <param name="color">Цвет, которым будет выделен отображаемый файл.</param>
        private void Tree(TreeView treeView, string path, char pathSeparator, Color color)
        {
            TreeNode lastNode = null;
            string subPathAgg;
            subPathAgg = string.Empty;

            foreach (string subPath in path.Split(pathSeparator))
            {
                subPathAgg += subPath + pathSeparator;
                TreeNode[] nodes = treeView.Nodes.Find(subPathAgg, true);
                if (nodes.Length == 0)
                    if (lastNode == null)
                    {
                        panel3.BeginInvoke(new MethodInvoker(delegate
                        {
                            lastNode = treeView.Nodes.Add(subPathAgg, subPath);
                            lastNode.ForeColor = color;
                            lastNode.Expand();
                            lastNode.EnsureVisible();
                        }));
                            System.Threading.Thread.Sleep(15);
                        }
                    else
                    {
                        panel3.BeginInvoke(new MethodInvoker(delegate
                        {
                            lastNode = Stick(lastNode, subPathAgg, subPath);
                            lastNode.ForeColor = color;
                            lastNode.Expand();
                            lastNode.EnsureVisible();
                        }));
                            System.Threading.Thread.Sleep(15);
                    }
                else
                {
                    lastNode = nodes[0];
                }
            }
        }

        private TreeNode Stick(TreeNode lastNode, string subPathAgg, string subPath)
        {
            return lastNode.Nodes.Add(subPathAgg, subPath);
        }
        
        /// <summary>
        /// Перегрузка. Возврящает все директории и файлы находящиеся в указаной директории.
        /// </summary>
        /// <param name="path">Имя начальной директории</param>
        /// <param name="pattern">Маска по которой будет выполняться поиск файлов</param>
        /// <returns>Список всех файлов и директорий</returns>
        private List<string> GetFiles(string path, string pattern)
        {
            var files = new List<string>();

            try
            {
                files.AddRange(Directory.GetDirectories(directory.Text, "*.*", SearchOption.TopDirectoryOnly).ToList());
                files.AddRange(Directory.GetFiles(path, pattern, SearchOption.TopDirectoryOnly));
                foreach (var directory in Directory.GetDirectories(path))
                    files.AddRange(GetFiles(directory, pattern));
            }
            catch (UnauthorizedAccessException) { }

            return files;
        }

        private static void SaveTree(TreeView tree, string filename)
        {
            using (Stream file = File.Open(filename, FileMode.Create))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(file, tree.Nodes.Cast<TreeNode>().ToList());
            }
        }

        private static void LoadTree(TreeView tree, string filename)
        {
            if (File.Exists(filename))
            {
                using (Stream file = File.Open(filename, FileMode.Open))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    object obj = bf.Deserialize(file);

                    TreeNode[] nodeList = (obj as IEnumerable<TreeNode>).ToArray();
                    tree.Nodes.AddRange(nodeList);
                    tree.ExpandAll();
                }               
            }

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

                Settings.Default["pathNameLoad"] = directory.Text;
                Settings.Default["searchingNameLoad"] = fileName.Text;
                Settings.Default["content"] = content.Text;
                SaveTree(treeView1, "tree.xml");
                Settings.Default["pause"] = false;


            if (pause)
                {
                    Settings.Default["pause"] = true;
                    Settings.Default["allFilesSize"] = massFilesSize;
                    Settings.Default["searchedSize"] = numberFile;
                    Settings.Default["timer"] = time;      

                    StringCollection str = new StringCollection();
                    foreach (string item in forSave)
                    {
                        str.Add(item);
                    }
                    Settings.Default["searchedFilesCollection"] = str;
                }
                else
                {
                    Settings.Default["pause"] = false;
                }
                Settings.Default.Save();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            directory.Text = Settings.Default["pathNameLoad"].ToString();
            fileName.Text = Settings.Default["searchingNameLoad"].ToString();
            content.Text = Settings.Default["content"].ToString();
            pause = Convert.ToBoolean(Settings.Default["pause"]);
            if (pause)
            {
                pauseButton.Text = "Продолжить";
                pauseButton.Visible = true;

                massFilesSize = Convert.ToInt32(Settings.Default["allFilesSize"]);
                numberFile = Convert.ToInt32(Settings.Default["searchedSize"]);
                time = Convert.ToDateTime(Settings.Default["timer"]);

                dynamic list = Settings.Default["searchedFilesCollection"];
                foreach (string item in list)
                {
                    forSave.Add(item);
                }

                LoadTree(treeView1, "tree.xml");

                progressBar.Minimum = 1;
                progressBar.Maximum = massFilesSize;
                progressBar.Value = numberFile;
                progressBar.Step = 1;
                progressBar.Visible = true;

                labelForFileNumber.Text = numberFile.ToString() + " из " + progressBar.Maximum.ToString();
                labelForFileNumber.Visible = true;

                labelForFilename.Visible = true;

                labelTimer.Text = string.Format("{0:HH:mm:ss}", time);
                labelTimer.Visible = true;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((pauseButton.Text == "Пауза") && (pauseButton.Visible == true))
            {
                PauseClick(pauseButton);
            }
            DialogResult dialogResult = MessageBox.Show("Уверены, что хотите закрыть приложение?", "Завершение поиска", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
            }
            else
            {
                e.Cancel = true;
                //PauseClick(pauseButton);
            }
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            PauseClick(pauseButton);
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.FullPath.Contains("."))
                System.Diagnostics.Process.Start(e.Node.FullPath);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            time = time.AddSeconds(1);
            labelTimer.Text = string.Format("{0:HH:mm:ss}", time);
        }
    }
}
